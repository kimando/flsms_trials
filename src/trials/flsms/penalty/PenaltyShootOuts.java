/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trials.flsms.penalty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kimando
 */
public class PenaltyShootOuts 
{
    public void getWinner(String player1Shots, String player1dives, String player2Shots, String player2dives)
    {
        String theDives1[] = player1dives.split(",");
        String theShots1[] = player1Shots.split(",");
        String theDives2[] = player2dives.split(",");
        String theShots2[] = player2Shots.split(",");
        
        int player1 = 0;
        int player2 = 0;
        
        if(theDives1.length == 5 && theShots1.length == 5 && theDives2.length == 5 && theShots2.length == 5)
        {
            if(theDives1.length == theShots1.length || theDives2.length == theShots2.length)
            {                
                for(int i = 0; i < theShots1.length; i++)
                {
                    if(Integer.parseInt(theShots1[i]) != Integer.parseInt(theDives2[i]))
                    {
                        player1++;
                    }
                    
                    if(Integer.parseInt(theShots2[i]) != Integer.parseInt(theDives1[i]))
                    {
                        player2++;
                    }
                }
                
                goalsDisplay(player1, player2);
            }
            else
            {
                System.out.println("The shots must be equal to the dives");
            }
        }
        else
        {
            System.out.println("All shots and dives must be five to get the winner");
        }
    }
    
    private void goalsDisplay(int player1, int player2)
    {
        System.out.println("\nPlayer 1: "+player1+" - Player 2: "+player2);
        
        if(player1 == player2){System.out.println("Player 1 draws with player 2");}
        if(player1 > player2){System.out.println("Player 1 wins");}
        if(player1 < player2){System.out.println("Player 2 wins");}        
    }
    
    public static void main(String [] args)
    {
        PenaltyShootOuts penaltyShootOuts = new PenaltyShootOuts();
        String input = "";
            
        while(!input.equalsIgnoreCase("END"))
        {
            try 
            {
                System.out.println("\n\nPenalty shoot outs. "
                        + "\nDives: 1. LEFT, 2. CENTER, 3. RIGHT, "
                        + "\nShots: 1. LEFT, 2. CENTER, 3. RIGHT, "
                        + "\nType END to quit");
                System.out.println("Player 1: For the five shots, enter dives separated by commas e.g 1, 2, 2, 1, 3");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String player1dives = bufferRead.readLine();
                input = player1dives;
                System.out.println("Player 2: For the five shots, enter shots separated by commas e.g 3, 2, 1, 2, 1");
                String player2Shots = bufferRead.readLine();
                input = player2Shots;  
                
                System.out.println("Player 2: For the five shots, enter dives separated by commas e.g 1, 2, 2, 1, 3");
                String player2dives = bufferRead.readLine();
                input = player1dives;
                System.out.println("Player 1: For the five shots, enter shots separated by commas e.g 3, 2, 1, 2, 1");
                String player1Shots = bufferRead.readLine();
                input = player2Shots; 
                
                penaltyShootOuts.getWinner(player1Shots, player1dives, player2Shots, player2dives);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(PenaltyShootOuts.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
